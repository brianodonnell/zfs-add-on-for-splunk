import json
import datetime
import subprocess
import re
import sys

# Import our helpers
from zfs_helpers import *

SCHEME = """<scheme>
  <title>ZFS status</title>
  <description>Get data from zpool status command.</description>
  <use_external_validation>false</use_external_validation>
  <streaming_mode>simple</streaming_mode>

  <endpoint>
    <args>
      <arg name="zpool_list">
        <title>Zpool List</title>
        <description>A space delimited list of one or more zpools to investigate with the zfs status command. Use ALL__POOLS to specify using all zpools returned by zpool list.</description>
        <validation>
          validate(isstr('zpool_list'),"zpool_list is not a string")
        </validation>
      </arg>
    </args>
  </endpoint>
</scheme>
"""

def do_scheme():
  print SCHEME

# Routine to get the value of an input
def get_status():
  try:
    pools_out = get_pools()

    for pool_name in pools_out:
      status_cmd = ['zpool', 'status', pool_name]
      status_obj = subprocess.Popen(status_cmd,stdout=subprocess.PIPE).stdout.read()
      status_comp = re.compile("^\s*(\w+?)\:\s*(.+?)\n+\t*\s*\w*?\:*^\s*state:\s(?P<state_o>.+?)$\s*scan:\s(scrub\srepaired|resilvered)\s(?P<scrub_bytes_o>[0-9.]+?)[BKMGTP]\sin\s(?P<scrub_duration_o>.+?)\swith\s(?P<scrub_errors_o>\d+?)\serrors\son\s(?P<scrub_date_o>.+?\d{4})$\s*config:.+?errors:\s*(?P<errors_o>.+?)$", re.MULTILINE | re.DOTALL)
      status_vars = status_comp.search(status_obj)
      timestamp = datetime.datetime.now().isoformat()
      # Mon Sep 17 07:11:42 2018
      status_scrub_date = datetime.datetime.strptime(status_vars.group('scrub_date_o'), '%c').isoformat()
      # 5h7m
      duration_search = re.search('(\d+?)h(\d+?)m', status_vars.group('scrub_duration_o'))
      dur_s = None
      if duration_search:
        dur_h = int(duration_search.group(1))
        dur_m = int(duration_search.group(2))
        dur_s = (dur_h * 3600) + (dur_m * 60)

      if len(status_vars.groups()) == 9:
        log_obj = {
          '_time': timestamp,
          'pool': pool_name,
          'state': status_vars.group('state_o'),
          'scrub_date': status_scrub_date,
          'scrub_bytes': status_vars.group('scrub_bytes_o'),
          'scrub_duration': dur_s,
          'scrub_errors': status_vars.group('scrub_errors_o'),
          'errors': status_vars.group('errors_o')
        }

        print json.dumps(log_obj)

      else:
        sys.exit("zpool status had unrecognized output")

  except Exception, e:
    raise Exception, "Error getting Splunk configuration via STDIN: %s" % str(e)

# Script must implement these args: scheme, validate-arguments
if __name__ == '__main__':
  if len(sys.argv) > 1:
    if sys.argv[1] == "--scheme":
      do_scheme()
    elif sys.argv[1] == "--validate-arguments":
      validate_arguments()
    else:
      print "You giveth weird arguements"

  else:
    # dewit
    get_status()

  sys.exit(0)
