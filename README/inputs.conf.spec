[zfs_status://<name>]
* Configure an input for collecting from 'zpool status'

zpool_list = <value>
* A single zpool name or list of zpool names for monitoring. 'ALL__POOLS' will monitor all available pools.

[zfs_iostat://<name>]
* Configure an input for collecting from 'zpool iostat'

zpool_list = <value>
* A single zpool name or list of zpool names for monitoring. 'ALL__POOLS' will monitor all available pools.

[zfs_iostat_metrics://<name>]
* Configure an input for collecting metrics from 'zpool iostat'

zpool_list = <value>
* A single zpool name or list of zpool names for monitoring. 'ALL__POOLS' will monitor all available pools.
